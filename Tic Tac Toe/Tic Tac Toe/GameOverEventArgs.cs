﻿using System;

namespace Tic_Tac_Toe
{
    public class GameOverEventArgs : EventArgs
    {
        public bool IsATie { get; set; }
        public string NameOfWinner { get; set; }
    }
}