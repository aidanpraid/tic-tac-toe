﻿using PropertyChanged;

namespace Tic_Tac_Toe.Model
{
    [ImplementPropertyChanged]
    public class Cell
    {
        #region Ctor

        public Cell(int cellNumber)
        {
            CellNumber = cellNumber;
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Number of cell
        /// </summary>
        public int CellNumber { get; private set; }

        public string PlayerName { get; set; }

        /// <summary>
        ///     Tells us that this cell is contained in "win streak"
        /// </summary>
        public bool IsPartOfAWin
        {
            get;
            set;
        }

        #endregion Properties
    }
}