﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using PropertyChanged;
using Tic_Tac_Toe.Model;
using Tic_Tac_Toe.Properties;

namespace Tic_Tac_Toe.ViewModel
{
    [ImplementPropertyChanged]
    public class TicTacToeViewModel
    {

        /// <summary>
        /// All possible winning combinations of cells
        /// </summary>
        private static readonly int[][] WinningCombinations = {
            new []{0,1,2},
            new []{3,4,5},
            new []{6,7,8},
            new []{0,3,6},
            new []{1,4,7},
            new []{2,5,8},
            new []{0,4,8},
            new []{2,4,6}
        };

        #region Ctor

        public TicTacToeViewModel()
        {
            Cells = new ObservableCollection<Cell>();
            for (var i = 0; i < 9; i++)
                Cells.Add(new Cell(i));
            CurrentPlayerName = "X";
            GameOverEvent += TicTacToeViewModel_GameOverEvent;
        }

        #endregion

        #region Fields

        private RelayCommand _moveCommand;
        private RelayCommand _newGameCommand;

        #endregion

        #region Command

        /// <summary>
        ///     Represents a request to move on a given cell
        /// </summary>
        public ICommand MoveCommand
        {
            get
            {
                return _moveCommand ?? (_moveCommand = new RelayCommand(
                    cellnumber => Move(Convert.ToInt32(cellnumber)),
                    cellnumber => Cells[Convert.ToInt32(cellnumber)].PlayerName == null && !IsGameOver));
            }
        }

        /// <summary>
        ///     Represents a request to start new game
        /// </summary>
        public ICommand NewGameCommand
        {
            get { return _newGameCommand ?? (_newGameCommand = new RelayCommand(cellnumber => NewGame())); }
        }

        #endregion

        #region Properties

        /// <summary>
        ///     Tells that game is over
        /// </summary>
        public bool IsGameOver { get; set; }

        /// <summary>
        ///     Collection of Cells
        /// </summary>
        public ObservableCollection<Cell> Cells { get; set; }

        /// <summary>
        ///     Gets current name of a player
        /// </summary>
        public string CurrentPlayerName { get; set; }

        #endregion

        #region Methods

        public void NewGame()
        {
            foreach (var cell in Cells)
            {
                cell.PlayerName = null;
                cell.IsPartOfAWin = false;
            }
            IsGameOver = false;

            CurrentPlayerName = "X";
        }

        private void Move(int cellNumber)
        {
            Cells[cellNumber].PlayerName = CurrentPlayerName;


            //After move we checks current situation on board to decide whether we have winner or tie 
            if (HaveWinner(CurrentPlayerName))
            {
                RaiseGameOverEvent(new GameOverEventArgs
                {
                    IsATie = false,
                    NameOfWinner = CurrentPlayerName
                });
            }
            else if (TieGame())
            {
                RaiseGameOverEvent(new GameOverEventArgs { IsATie = true });
            }

            // If the game isn't over, switch the current player    
            else
            {
                CurrentPlayerName = CurrentPlayerName.Equals("X") ? "0" : "X";
            }
        }

        /// <summary>
        ///     Checks if we have a winner
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        private bool HaveWinner(string player)
        {
            foreach (var match in WinningCombinations)
            {
                if (match.Any(i => Cells[i].PlayerName != player))
                {
                    continue;
                }
                foreach (var i in match)
                {
                    Cells[i].IsPartOfAWin = true;
                }
                return true;

            }
            return false;
        }

        /// <summary>
        ///     Checks if game become tie
        /// </summary>
        /// <returns></returns>
        private bool TieGame()
        {
            var noMove = Cells.All(cell => cell.PlayerName != null);
            if (noMove)
                IsGameOver = true;
            return noMove;
        }

        #endregion

        #region EventHandlers


        private void RaiseGameOverEvent(GameOverEventArgs e)
        {
            var handler = GameOverEvent;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private static void TicTacToeViewModel_GameOverEvent(object sender, GameOverEventArgs e)
        {
            if (e.IsATie)
            {
                MessageBox.Show(Resources.MainWindow_NoWinner,
                    Resources.MainWindow_GameOver);
            }
            else
            {
                MessageBox.Show(
                    e.NameOfWinner +
                    Resources.MainWindow_Congratulations,
                    Resources.MainWindow_GameOver);
            }
        }

        #endregion EventHandlers

        #region Event
        public delegate void GameOverEventHandler(object sender, GameOverEventArgs e);

        public event GameOverEventHandler GameOverEvent;

        #endregion
    }
}